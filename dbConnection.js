/**
 * Created by Jacob on 23-12-2014.
 */



this.connect = function connect() {
    connection.connect(function (err) {
        if (err) {
            console.error('error connecting to database: ' + err.stack);
            return;
        }
    });
}

this.executeQuery = function executeQuery(query, value) {
    // TODO implement in a second!
}

// Request player skills
findSkillsByCharacterId = function(characterId) {
    console.log("skills request from character with id: " + characterId);

    // Establish connection
    this.connect();

    // Get player skills
    var value = characterId,
        query = 'select level as skilllevel, skills.name as skillname, skills.description, skills.skillpower, skills.type, skills.cooldownTime from characters_has_skills ' +
            'join skills on characters_has_skills.skillID=skills.id where characters_has_skills.characterID=?'
    connection.query(query, [value], function (err, results) {
        if (err) {
            console.error('error query: ' + err.stack);
            return;
        }

        this.emit("response skills", results);
    });

    connection.end();
}

// Request player character
findCharacterById = function(characterId) {
    console.log("character request with id: " + characterId);

    // Establish connection
    this.connect();

    // Get player data
    var value = characterId,
        query = 'select characters.id as userId,characters.name as name, health, mana, level, characters.skillpower as playerSkillpower, modelid, characters.classid from characters' +
            ' join users on users.id=characters.userid where characters.id=?'
    connection.query(query, [value], function (err, rows) {
        if (err) {
            console.error('error query: ' + err.stack);
            return;
        }

        this.emit("response character data", {characterData: rows});
    });

    connection.end();
}

exports.dbConnection = dbConnection;