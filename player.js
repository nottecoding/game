var player = function(userId, socketId, name, health, mana, level, skillpower, skills, modelId, classId, buildingId, xCord, yCord){
    this.userId = userId;
    this.socketId = socketId;

    this.name = name;

    this.health = health;
    this.mana = mana;
    this.level = level;

    this.skillpower = skillpower;
    this.skills = skills;

    this.modelId = modelId;
    this.classId = classId;
    this.buildingId = buildingId;

    this.xCord = xCord;
    this.yCord = yCord;


    // Getters and setters
    this.getUserId = function getUserId() {
        return this.userId;
    }

    this.getSocketId = function getSocketId() {
        return this.socketId;
    }

    this.getName = function getName() {
        return this.name;
    }

    this.getHealth = function getHealth() {
        return this.health;
    }

    this.getMana = function getMana() {
        return this.mana;
    }

    this.getLevel = function getLevel() {
        return this.level;
    }

    this.getSkillpower = function getSkillpower() {
        return this.skillpower;
    }

    this.getSkills = function getSkills() {
        return this.skills;
    }

    this.getModelId = function getModelId() {
        return this.modelId;
    }

    this.getClassId = function getClassId() {
        return this.classId
    }

    this.getBuildingId = function getBuildingId() {
        return this.buildingId;
    }

    this.getX = function getX() {
        return this.xCord;
    }

    this.getY = function getY() {
        return this.yCord;
    }

    this.setName = function setName(newName) {
        this.name = newName;
    }

    this.setHealth = function setHealth(newHealth) {
        this.health = newHealth;
    }

    this.setMana = function setMana(newMana) {
        this.mana = newMana;
    }

    this.setLevel = function setLevel(newLevel) {
        this.level = newlevel;
    }

    this.setSkillpower = function setSkillpower(newSkillpower) {
        this.skillpower = newSkillpower;
    }

    this.setX = function setX(newX) {
        this.xCord = newX;
    };

    this.setY = function setY(newY) {
        this.yCord = newY;
    };

    this.setBuildingId = function setBuildingId(newBuildingId) {
        this.buildingId = newBuildingId;
    }
};

exports.player = player;