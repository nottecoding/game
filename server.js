/***************************************************
 ** Node.JS Requirements
 ****************************************************/
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var player = require("./player").player;
var path = require('path');
var mysql = require('mysql');

/***************************************************
 ** Game variables
 ****************************************************/
var players;

/***************************************************
 ** Game initialisation
 ****************************************************/
function init() {
    // Create an empty array to store players
    players = [];

    // listen to port 3000 for new connections
    server.listen(3000);
    console.log('listening on port 3000...');

    // Start listening for events
    setEventHandlers();
};

/***************************************************
 ** Game event handler
 ****************************************************/
var setEventHandlers = function() {
    // Socket.IO
    io.on("connection", onSocketConnection);
    console.log('game initialized');
};

// New socket connection
function onSocketConnection(client) {
    console.log("New player has connected: "+ client.id);

    // Listen for client disconnected
    client.on("disconnect", onClientDisconnect);

    // Listen for new player message
    client.on("new player", onNewPlayer);

    // Listen for move player message
    client.on("move player", onMovePlayer);

    // Listen for move player message
    client.on("stop player", onStopPlayer);

    // Request player skills
    client.on("request skills", onSkillRequest);

    // Request character data
    client.on("request character data", onCharacterRequest);

    // Listen for players who changed building
    client.on("player building changed", onBuildingChanged);

    // Listen for chatmessages
    client.on("send message", onMessageReceived);

    // Listen for skill events
    client.on("player skill", onPlayerSkillReceived);

    client.on("request player duel", onRequestDuel);

    client.on("reply challenge", onReplyDuel);

    // Listen for health messages
    client.on("player got hit", onPlayerHit);

    client.on("request quest data", onQuestDataRequest);
    client.on("save quest progress", onQuestSave);
    client.on("save quest objects", onQuestObjectSave);

}

// Socket client has disconnected
function onClientDisconnect() {
    var removePlayer = playerBySocketId(this.id);

    // Player not found
    if (!removePlayer) {
        console.log("Player not found: "+this.id);
        return;
    }

    saveCharacterData(removePlayer.userId, removePlayer.getX(), removePlayer.getY(), removePlayer.getBuildingId());

    // Remove player from players array
    players.splice(players.indexOf(removePlayer), 1);

    // Broadcast removed player to connected socket clients
    this.broadcast.emit("remove player", {userId: removePlayer.getUserId(), name: removePlayer.getName()});

    console.log("Player has disconnected with userId: " + removePlayer.getUserId());
    console.log("Player count: " + players.length);
}

// New player has joined
function onNewPlayer(data) {
    // Create a new player
    console.log("new player");
    var newPlayer = new player(data.userId, this.id, data.name, data.health, data.mana, data.level, data.skillpower, data.skills, data.modelId, data.classId, data.buildingId, data.x, data.y);

    // Broadcast new player to connected socket clients
    this.broadcast.emit("new player", {userId: newPlayer.getUserId(), name: newPlayer.getName(), health: newPlayer.getHealth(), mana: newPlayer.getMana(), level: newPlayer.getLevel(), skillpower: newPlayer.getSkillpower(), skills: newPlayer.getSkills(), modelId: newPlayer.getModelId(), classId: newPlayer.getClassId(), x: newPlayer.getX(), y: newPlayer.getY(), buildingId: newPlayer.getBuildingId()});

    // Send existing players to the new player
    var i, existingPlayer;
    for (i = 0; i < players.length; i++) {
        existingPlayer = players[i];
        this.emit("new player", {userId: existingPlayer.getUserId(), name: existingPlayer.getName(), health: existingPlayer.getHealth(), mana: existingPlayer.getMana(), level: existingPlayer.getLevel(), skillpower: existingPlayer.getSkillpower(), skills: existingPlayer.getSkills(), modelId: existingPlayer.getModelId(), classId: existingPlayer.getClassId(), x: existingPlayer.getX(), y: existingPlayer.getY(), buildingId: existingPlayer.getBuildingId()});
    }

    // Add new player to the players array
    players.push(newPlayer);
    console.log("Player has connected with userId: " + newPlayer.getUserId());
    console.log("Player count: " + players.length);
}

// Player has moved
function onMovePlayer(data) {
    // Find player in array
    var movePlayer = playerById(data.userId);

    // Player not found
    if (!movePlayer) {
        console.log("Player not found: " + data.userId);
        return;
    }

    // Update player position
    movePlayer.setX(data.x);
    movePlayer.setY(data.y);

    // Broadcast updated position to connected socket clients
    this.broadcast.emit("move player", {
        userId: movePlayer.getUserId(),
        x: movePlayer.getX(),
        y: movePlayer.getY(),
        frame: data.frame,
        direction: data.direction
    });
}

function onStopPlayer(data) {
    // Find player in array
    var stopPlayer = playerById(data.userId);

    // Player not found
    if (!stopPlayer) {
        console.log("Player not found: " + data.userId);
        return;
    }

    // Update player position
    stopPlayer.setX(data.x);
    stopPlayer.setY(data.y);

    // Broadcast updated position to connected socket clients
    this.broadcast.emit("stop player", {
        userId: stopPlayer.getUserId(),
        x: stopPlayer.getX(),
        y: stopPlayer.getY(),
        frame: data.frame,
        direction: data.direction
    });
}

// Socket client has disconnected
function onBuildingChanged(data) {
    // Find player in array
    var movePlayer = playerById(data.userId);

    // Player not found
    if (!movePlayer) {
        console.log("Player not found: " + data.userId);
        return;
    };

    // Update player buildingId
    movePlayer.setBuildingId(data.buildingId);

    // Broadcast removed player to connected socket clients
    this.broadcast.emit("player building changed", {
        userId: movePlayer.getUserId(),
        buildingId: movePlayer.getBuildingId()
    });

    console.log("Player with userId " + data.userId + " changed to building with id " + data.buildingId);
};


// Request player skills
function onSkillRequest(data) {
    findSkillsByCharacterId(data.characterId);
}

// Request player character
function onCharacterRequest(data) {
    findCharacterById(data.characterId);
}


function onMessageReceived(data){
    var dt = new Date();
    this.broadcast.emit("receive message", {name: data.name, chatMessage: data.msg, time: dt.getHours() +':' + dt.getMinutes()});
}

// Player did a skill
function onPlayerSkillReceived(data){
    this.broadcast.emit("player skill", {sendToUserId: data.sendToUserId, userId: data.userId, mana: data.mana, x: data.x, y: data.y, direction: data.direction, characterLevel: data.characterLevel, skill: data.skill});
}

function onRequestDuel(data) {
    this.broadcast.emit("request player duel", { challengerId: data.challengerId, challengedId: data.challengedId});
}

function onReplyDuel(data) {
    this.broadcast.emit("reply challenge", data);
}

function onPlayerHit(data) {
    this.broadcast.emit("player got hit", data);
}

setInterval(addManaToPlayers, 50);

function addManaToPlayers() {
    io.emit("add mana", {amount: 0.5});
}

function onQuestDataRequest(data) {
    findAllQuestsForCharacterId(data.characterId);
    findAllQuestObjectsForCharacterId(data.characterId);
}

function onQuestSave(data) {
    saveQuestProgress(data.characterId, data.questId, data.doingQuest, data.finishedQuest);
}

function onQuestObjectSave(data) {
    saveQuestObjectProgress(data.characterId, data.questObjectId, data.objectFound, data.visible, data.loaded);
}

/**************************************************
 ** GAME HELPER FUNCTIONS
 **************************************************/
// Find player by ID
function playerById(id) {
    var i;
    for (i = 0; i < players.length; i++) {
        if (players[i].getUserId() == id) {
            return players[i];
        }
    };

    return false;
};

function playerBySocketId(id) {
    var i;

    for (i = 0; i < players.length; i++) {
        if (players[i].getSocketId() == id) {
            return players[i];
        }
    };

    return false;
}

/**************************************************
 ** RUN THE GAME
 **************************************************/

init();

app.use(express.static(path.join(__dirname, 'Client')));

console.log(express.static(path.join(__dirname, 'Client')));

app.get('/', function(req, res){
    res.sendFile('/index.html');
});

/**************************************************
 ** DB CONNECTION
 **************************************************/
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'homestead',
    password: 'secret',
    database: 'odiseerpg'
});

// Request player skills
var findSkillsByCharacterId = function(characterId) {
    console.log("Skills request from player with userId: " + characterId);

    // Get player skills
    var query = 'select id, level as skilllevel, skills.name as skillname, skills.description, skills.skillpower, skills.mana as skillMana,  skills.type, skills.cooldownTime from characters_has_skills ' +
        'join skills on characters_has_skills.skillID=skills.id where characters_has_skills.characterID=?';

    connection.query(query, [characterId], function (err, results) {
        if (err) {
            console.error('error query: ' + err.stack);
            return;
        }

        io.emit("request skills", results);
    });
}

// Request player character
var findCharacterById = function(characterId) {
    console.log("Player request with userId: " + characterId);

    // Get player data
    var query = 'select characters.id as userId,characters.name as name, health, mana, level, characters.skillpower as playerSkillpower, modelid, characters.classid, buildingId, x, y from characters' +
        ' join users on users.id=characters.userid where characters.id=?';

    connection.query(query, [characterId], function (err, results) {
        if (err) {
            console.error('error query: ' + err.stack);
            return;
        }

        io.emit("response character data", results[0]);
        console.log("Player data send");
    });
}

// Save player character
var saveCharacterData = function(characterId, x, y, buildingId) {
    // Save player data
    var query = 'UPDATE characters ' +
        'SET characters.x=?, characters.y=?, characters.buildingId=? ' +
        'WHERE characters.id=? ';

    connection.query(query, [x, y, buildingId, characterId], function (err) {
        if (err) {
            console.error('error query: ' + err.stack);
            return;
        }
    });

    console.log("Player saved with userId: " + characterId);
}

var findAllQuestsForCharacterId = function(characterId) {
    var query = 'SELECT questes.questId, questes.buildingId, questes.modelId, questes.xCord, questes.yCord, characters_has_quests.doingQuest, characters_has_quests.finishedQuest FROM questes ' +
        'join characters_has_quests on questes.questId=characters_has_quests.questId where characters_has_quests.characterId=?';

    connection.query(query, [characterId], function (err, results) {
        if (err) {
            console.error('error query: ' + err.stack);
            return;
        }
        console.log(results);
        io.emit("response quest data", results);
    });
}

var findAllQuestObjectsForCharacterId = function(characterId) {
    var query = 'SELECT questobjects.objectId, questobjects.questId, characters_has_questobjects.objectFound, questobjects.xCord, questobjects.yCord, questobjects.buildingId, questobjects.modelName, characters_has_questobjects.visible, characters_has_questobjects.loaded FROM questobjects ' +
        'join characters_has_questobjects on questobjects.objectId=characters_has_questobjects.questObjectId where characters_has_questObjects.characterId=?';

    connection.query(query, [characterId], function (err, results) {
        if (err) {
            console.error('error query: ' + err.stack);
            return;
        }
        io.emit("response questObject data", results);
    });
}

var saveQuestProgress = function(characterId, questId, doingQuest, finishedQuest) {
    var query = 'UPDATE characters_has_quests ' +
        'SET doingQuest=?, finishedQuest=? ' +
        'WHERE characterId=? and questId=?';

    connection.query(query, [doingQuest, finishedQuest, characterId, questId], function (err) {
        if (err) {
            console.error('error query: ' + err.stack);
            return;
        }
    });
}

var saveQuestObjectProgress = function(characterId, questObjectId, objectFound, visible, loaded) {
    var query = 'UPDATE characters_has_questobjects ' +
        'SET objectFound=?, visible=?, loaded=? ' +
        'WHERE characterId=? and questObjectId=?';

    connection.query(query, [objectFound, visible, loaded, characterId, questObjectId], function (err) {
        if (err) {
            console.error('error query: ' + err.stack);
            return;
        }
    });
}