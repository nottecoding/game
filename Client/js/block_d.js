function block_d () {
    buildingId = 3;
    background = game.add.sprite(0, 0, 'block_d_inside');

    var doors;

    this.init = function init() {
        localPlayer.changeBuilding(3, 1200, game.height-75);

        doors = [];
        this.addwallsGroup();
        this.addDoors();
        this.addInnerwallsGroup();
        this.addMiscellaneous();
        this.addQuests();
    };

    this.addwallsGroup = function allwallsGroup() {
        var wall = wallsGroup.create(180, 327, 'left_vertical_wall');
        wall.body.immovable = true;
        wall = wallsGroup.create(184, 327, 'top_left_inner_wall');
        wall.body.immovable = true;
        wall = wallsGroup.create(183, 775, 'left_outer_wall');
        wall.body.immovable = true;
        wall = wallsGroup.create(1059, 775, 'bottom_left_vertical_wall');
        wall.body.immovable = true;
        wall = wallsGroup.create(1062, 1375, 'bottom_outer_wall');
        wall.body.immovable = true;
        wall = wallsGroup.create(1836, 1199, 'bottom_right_vertical_wall');
        wall.body.immovable = true;
        wall = wallsGroup.create(1839, 1199, 'bottom_right_outer_wall');
        wall.body.immovable = true;
        wall = wallsGroup.create(2351, 0, 'right_vertical_wall');
        wall.body.immovable = true;
        wall = wallsGroup.create(2132, 0, 'top_wall');
        wall.body.immovable = true;
        wall = wallsGroup.create(2130, 0, 'top_left_wall');
        wall.body.immovable = true;
        wall = wallsGroup.create(182, 327, 'top_left_inner_wall');
        wall.body.immovable = true;
        wall.scale.x = 1.05;
        wall = wallsGroup.create(1223, 773, 'courtyard_outer_wall');
        wall.body.immovable = true;
        wall = wallsGroup.create(1221, 773, 'side_courtyard_wall');
        wall.body.immovable = true;
        wall = wallsGroup.create(1828, 773, 'side_courtyard_wall');
        wall.body.immovable = true;
        wall = wallsGroup.create(1221, 884, 'bottom_courtyard_wall');
        wall.body.immovable = true;
    };

    this.addDoors = function addDoors() {
        doors.push(new entrance('door_d', 1200, game.height - 66 , 'mainmap', 'brickdoor'));

        doors.forEach(function(door){
            var entrance = entrances.create(door.getX(), door.getY(), door.getModel());
            entrance.body.immovable = true;
            entrance.scale.y = 2.64;
        });

        game.world.bringToTop(entrances);
    };

    this.addInnerwallsGroup = function addInnerwallsGroup() {
        // left corridor horizontal wallsGroup
        for (var i = 180; i < 2000; i+=350)
        {
            var wall = wallsGroup.create(i, 640, 'top_wall');
            wall.body.immovable = true;
            wall.scale.x = 1.35;
        }

        // left corridor vertical wallsGroup
        for (var i = 600; i < 2000; i+=400)
        {
            var wall = wallsGroup.create(i, 327, 'left_vertical_wall');
            wall.body.immovable = true;
            wall.scale.y = 0.615;
        }

        // right corridor wall
        var wall = wallsGroup.create(2227, 327, 'left_vertical_wall');
        wall.body.immovable = true;
        wall.scale.y = 0.535;

        // library
        var wall = wallsGroup.create(1221, 884, 'left_vertical_wall');
        wall.body.immovable = true;
        wall.scale.y = 0.735;
        wall = wallsGroup.create(1222, 1250, 'top_wall');
        wall.body.immovable = true;
        wall = wallsGroup.create(1495, 1250, 'top_wall');
        wall.body.immovable = true;
        wall.scale.x = 1.55;
        wall = wallsGroup.create(1836, 884, 'left_vertical_wall');
        wall.body.immovable = true;
        wall.scale.y = 0.62;

        // audience
        var wall = wallsGroup.create(2227, 773, 'left_vertical_wall');
        wall.body.immovable = true;
        wall.scale.y = 0.4;
        wall = wallsGroup.create(2227, 1050, 'left_vertical_wall');
        wall.body.immovable = true;
        wall.scale.y = 0.3;
        wall = wallsGroup.create(1828, 773, 'top_wall');
        wall.body.immovable = true;
        wall.scale.x = 1.81;
    };

    this.addMiscellaneous = function addMiscellaneous() {
        // library bookshelfs
        for (var i = 1224; i < 1800; i+=51) {
            var bookshelf = miscellaneous.create(i, 890, 'bookshelf');
            bookshelf.body.immovable = true;
            bookshelf.scale.x = 1.05;
        }

        // library desks
        for (var i = 1295; i < 1800; i+=105) {
            var desk = miscellaneous.create(i, 1020, 'desk');
            desk.body.immovable = true;
            desk = miscellaneous.create(i, 1140, 'desk');
            desk.body.immovable = true;
        }

        // classroom desks
        for (var i = 250; i < 2200; i+=200) {
            var desk = miscellaneous.create(i, 400, 'desk_with_pcs');
            desk.body.immovable = true;
            desk = miscellaneous.create(i, 530, 'desk_with_pcs');
            desk.body.immovable = true;
        }

        game.world.bringToTop(miscellaneous);
    };

    this.checkEntranceCollision = function checkEntranceCollision() {
        game.physics.arcade.overlap(localPlayer.getModel(), entrances, enterBuilding, null, this);
    };

    function enterBuilding(player, entrance) {
        if (cursors.down.isDown) {
            doors.forEach(function (door) {
                if (entrance.position.x == door.getX() && entrance.position.y == door.getY()) {
                    loadMap(door.getLeadsTo());
                }
            });
        }
    }

    this.addQuests = function addQuests() {
        quest.userCanDoQuest();
        for (var i = 0; i < quest.getPossibleQuests().length; i++) {
            //quests.destroy();
            if(quest.getPossibleQuests()[i].getBuildingId() == localPlayer.getBuildingId()) {
                quests.add(quest.getPossibleQuests()[i].getModel());
            };
        };

        game.world.bringToTop(quests);
    };
}