function settings() {

	var skillKeys = [];

	skillKeys[0] = new skillKey(game.input.keyboard.addKey(Phaser.Keyboard.E));
	skillKeys[1] = new skillKey(game.input.keyboard.addKey(Phaser.Keyboard.R));
	skillKeys[2] = new skillKey(game.input.keyboard.addKey(Phaser.Keyboard.T));
	skillKeys[3] = new skillKey(game.input.keyboard.addKey(Phaser.Keyboard.Y));

	this.bindSkillToKey = function bindSkillToKey(skillKeyNumber, skill) {
		skillKeys[skillKeyNumber].setSkill(skill);
	};

	this.getSkillKeys = function getSkillKeys() {
		return skillKeys;
	};

	this.unsetSkillKeys = function unsetSkillKeys() {
		game.input.keyboard.removeKey(Phaser.Keyboard.E);
		game.input.keyboard.removeKey(Phaser.Keyboard.R);
		game.input.keyboard.removeKey(Phaser.Keyboard.T);
		game.input.keyboard.removeKey(Phaser.Keyboard.Y);
	}
}