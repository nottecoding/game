/**
 * Created by Jacob on 1-12-2014.
 */
function remotePlayers() {
    var allRemotePlayers = [],
        mayCollide = [];

    this.getAllRemotePlayers = function getAllRemotePlayers() {
        return allRemotePlayers;
    };

    this.setAllRemotePlayers = function setAllRemotePlayers(newAllRemotePlayers) {
        allRemotePlayers = newAllRemotePlayers;
    };

    this.addToAllRemotePlayers = function addToAllRemotePlayers(newPlayer) {
        if (this.findPlayerById(newPlayer.getUserId()) == false) {
            allRemotePlayers.push(newPlayer);

            // the object needed for colliding with other players
            var obj = {};
            obj[newPlayer.getUserId()] = true;
            mayCollide.push({userId: newPlayer.getUserId(), value: true});
        }
    };

    this.removeFromAllRemotePlayers = function removeFromAllRemotePlayers(id) {
        var removePlayer = this.findPlayerById (id);

        if (removePlayer != false) {
            allRemotePlayers.splice(allRemotePlayers.indexOf(removePlayer), 1);
            removePlayer.getModel().kill();

            var mayCollidePlayer = this.findMayCollideByUserId(removePlayer.getUserId());
            if (mayCollidePlayer != false) {
                mayCollide.splice(mayCollide.indexOf(mayCollidePlayer), 1);
            }
        }
    };

    this.drawAndCheckCollision = function drawAndCheckCollision() {
        for (var i in allRemotePlayers) {
            var model = allRemotePlayers[i].getModel();

            //update the models position
            model.position.x = allRemotePlayers[i].getX();
            model.position.y = allRemotePlayers[i].getY();

            if (allRemotePlayers[i].getBuildingId() == localPlayer.getBuildingId()) {
                model.revive();
                game.world.bringToTop(model);

                if (!challenge.getArenaOpen()) {
                    this.collisionDetect(allRemotePlayers[i]);
                }
            }
        }
    };

    this.collisionDetect = function collisionDetect(remotePlayer) {
        var boundsA = localPlayer.getModel().getBounds();
        var boundsB = remotePlayer.getModel().getBounds();

        var mayCollidePlayer = this.findMayCollideByUserId(remotePlayer.getUserId());

        if(Phaser.Rectangle.intersects(boundsA, boundsB)) {
            if (mayCollidePlayer.value != false) {
                mayCollidePlayer.value = false;
                chat.appendMessage("Press TAB to challenge " + remotePlayer.getName() + " for a duel.");
                challenge.opponent = remotePlayer;
                //challenge.opponent.init();
            } else {
                challenge.checkChallengeKey();
            }
        } else {
            mayCollidePlayer.value = true;
        }
    };

    this.movePlayer = function movePlayer(userId, x, y, frame, direction) {
        var movePlayer = this.findPlayerById(userId);

        if (movePlayer != false) {
            movePlayer.setX(x);
            movePlayer.setY(y);
            movePlayer.setFrame(frame)
            movePlayer.setDirection(direction);
            movePlayer.animateMovement();
        }
    };

    this.stopPlayer = function stopPlayer(userId, x, y, frame, direction) {
        var movePlayer = this.findPlayerById(userId);

        if (movePlayer != false) {
            movePlayer.setX(x);
            movePlayer.setY(y);
            movePlayer.setFrame(frame)
            movePlayer.setDirection(direction);
            movePlayer.animateMovement('stop');
        }
    };

    this.playerBuildingChanged = function playerBuildingChanged(userId, buildingId) {
        var movePlayer = this.findPlayerById(userId);

        if (movePlayer != false) {
            var model = movePlayer.getModel();
            movePlayer.setBuildingId(buildingId);

            if(movePlayer.getBuildingId() == localPlayer.getBuildingId()) {
                model.revive();
                game.world.bringToTop(model);
            } else {
                model.kill();
                game.world.sendToBack(model);
            }
        }
    };

    this.findPlayerById = function findPlayerById(userId) {
        for (var i = 0; i < allRemotePlayers.length; i++) {
            if (allRemotePlayers[i].getUserId() == userId) {
                return allRemotePlayers[i];
            }
        }
        return false;
    };

    this.findMayCollideByUserId = function findMayCollideByUserId(userId) {
        for (var i = 0; i < mayCollide.length; i++) {
            if (mayCollide[i].userId == userId) {
                return mayCollide[i];
            }
        }

        return false;
    };
}