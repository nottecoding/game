function questObjects(objectId, questId, objectFound, xCord, yCord, buildingId, model, visible, loaded) {
	this.objectId = objectId;
	this.questId = questId;
	this.objectFound = objectFound;
	this.xCord = xCord;
	this.yCord = yCord;
	this.buildingId = buildingId;
	this.model = model;
	this.visible = visible;
	this.loaded = loaded;

	this.getObjectId = function getObjectId() {
		return this.objectId;
	};

	this.getQuestId = function getQuestId() {
		return this.questId;
	};

	this.getObjectFound = function getObjectFound () {
		return this.objectFound;
	};

	this.getXCord = function getXCord () {
		return this.xCord;
	};

	this.getYCord = function getYCord () {
		return this.yCord;
	};

	this.getBuildingId = function getBuildingId () {
		return this.buildingId;
	};

	this.getModel = function getModel() {
		return this.model;
	};

	this.getVisible = function getVisible() {
		return this.visible;
	};

	this.getLoaded = function getLoaded() {
		return this.loaded;
	};

	this.setObjectFound = function setObjectFound(foundIt) {
		this.objectFound = foundIt;
	};

	this.setVisible = function setVisible(newVisible) {
		this.visible = newVisible;
	};

	this.setLoaded = function setLoaded(newLoaded) {
		this.loaded = newLoaded;
	};

	this.showQuestObjects = function showQuestObjects(questId) {
		if(this.buildingId == localPlayer.getBuildingId()) {
			if(this.visible) {
				if(!this.loaded && !this.objectFound) {
					if(questId == this.questId) {
						searchQuestsObjects.add(this.model);
						game.world.bringToTop(searchQuestsObjects);
						this.loaded = true;
					};
				};
			};
		} else {
			this.loaded = false;
		}
	};
}