/**
 * Created by Jacob on 27-11-2014.
 */
function entrance(name, xCord, yCord, leadsTo, model) {
    var name = name;
    var xCord = xCord;
    var yCord = yCord;
    var leadsTo = leadsTo;
    var model = model;

    this.getName = function getName() {
        return name;
    };

    this.getX = function getX() {
        return xCord;
    };

    this.getY = function getY() {
        return yCord;
    };

    this.getLeadsTo = function getLeadsTo() {
        return leadsTo;
    };

    this.getModel = function getModel() {
        return model;
    };
}