function characterClass(id, name, description, skillsId) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.skillsId = skillsId;

    this.getId = function getId() {
        return id;
    }

    this.getName = function getName() {
        return name;
    }

    this.getDescription = function getDescription() {
        return description;
    }

    this.getSkillsId = function getSkillsId() {
        return skillsId;
    }

}