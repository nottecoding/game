function skill(id, name, description, skillpower, mana, type, cooldownTime) {
    // Animation to the list
    this.id = id;
    this.name = name;
    this.description = description;
    this.skillpower = skillpower;
    this.mana = mana;
    this.type = type;
    this.cooldownTime = cooldownTime;
    var available = true;

    this.getId = function getId() {
        return id;
    };

    this.getName = function getName() {
        return name;
    };

    this.getDescription = function getDescription() {
        return description;
    };

    this.getSkillpower = function getSkillpower() {
        return skillpower;
    };

    this.getMana = function getMana() {
        return mana;
    };

    this.getType = function getType() {
        return type;
    };

    this.setSkillpower = function setSkillpower(newSkillpower) {
        skillpower = newSkillpower;
    };
}