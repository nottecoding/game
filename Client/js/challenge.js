function challenge () {
    this.opponent;
    var challengeKey = game.input.keyboard.addKey(Phaser.Keyboard.TAB);
    var arenaOpen = false;
    var challengeKeyReleased = true;

    this.getArenaOpen = function getArenaOpen() {
        return arenaOpen;
    };

    this.setArenaOpen = function setArenaOpen(newValue) {
        arenaOpen = newValue;
    };

    this.checkChallengeKey = function checkChallengeKey() {
        if (!arenaOpen) {
            if (challengeKey.isDown && challengeKeyReleased) {
                challengeKeyReleased = false;

                this.sendChallenge();
                console.log("challenged the player");
            }

            game.input.keyboard.onUpCallback = function (e) {
                if (e.keyCode == Phaser.Keyboard.TAB) {
                    challengeKeyReleased = true;
                }
            };
        }
    };

    this.sendChallenge = function sendChallenge() {
        socket.emit("request player duel", {challengerId: localPlayer.getUserId(), challengedId: this.opponent.getUserId()});
        var message = 'You have challenged ' + this.opponent.getName();
        chat.appendMessage(message);
    }
}
