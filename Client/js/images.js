function loadImages() {
    // backgrounds
    game.load.image('background', '/img/background-main.jpg');
    game.load.image('block_d_inside', '/img/buildings/block_d_inside/block_d_inside.jpg');
    game.load.image('background_arena', '/img/arena/background_arena.jpg');

    // player sprites
    game.load.spritesheet('player_model1', '/img/player/player_model1.png', 27, 32);
    game.load.spritesheet('player_model2', '/img/player/player_model2.png', 27, 32);
    game.load.spritesheet('player_model3', '/img/player/player_model3.png', 27, 32);
    game.load.spritesheet('player_model4', '/img/player/player_model4.png', 27, 32);

    // buildings
    game.load.image('block_b', '/img/buildings/block_b.png');
    game.load.image('block_d_tower', '/img/buildings/block_d_tower.png');
    game.load.image('block_d_left_wing', '/img/buildings/block_d_left_wing.png');
    game.load.image('block_d_right_wing', '/img/buildings/block_d_right_wing.png');
    game.load.image('block_d_bottom_right', '/img/buildings/block_d_bottom_right.png');
    game.load.image('block_e', '/img/buildings/block_e.png');
    game.load.image('block_m', '/img/buildings/block_m.png');

    // entrances
    game.load.image('brickdoor', '/img/interactions/doors/brickdoor.png');
    game.load.image('slidingdoor', '/img/interactions/doors/slidingdoor.png');
    game.load.image('bigSlidingdoor', '/img/interactions/doors/bigSlidingdoor.png');

    // flora
    game.load.image('tree', '/img/flora/tree.png');
    game.load.image('tree_small', '/img/flora/tree_small.png');
    game.load.image('flower', '/img/flora/flower.png');
    game.load.image('bush', '/img/flora/bush.png');

    // miscellaneous
    game.load.spritesheet('fountain', 'img/miscellaneous/outdoor/fountain.png', 90, 78);
    game.load.spritesheet('wind turbine', 'img/miscellaneous/outdoor/wind turbine.png', 53, 127);
    game.load.image('water_mainmap', 'img/miscellaneous/outdoor/water.png');
    game.load.image('bookshelf', 'img/miscellaneous/indoor/bookshelf.png');
    game.load.image('desk', 'img/miscellaneous/indoor/desk.png');
    game.load.image('desk_with_pcs', 'img/miscellaneous/indoor/desk_with_pcs.png');

    // arena
    game.load.image('arena_floor', '/img/arena/arena_floor.png');
    game.load.image('odisee_logo', '/img/arena/odisee_logo.png');
    game.load.image('horizontal_wall_top', '/img/arena/horizontal_wall_top.png');
    game.load.image('horizontal_wall_bottom', '/img/arena/horizontal_wall_bottom.png');
    game.load.image('vertical_wall_left', '/img/arena/vertical_wall_left.png');
    game.load.image('vertical_wall_right', '/img/arena/vertical_wall_right.png');
    game.load.image('corner_top_left', '/img/arena/corner_top_left.png');
    game.load.image('corner_top_right', '/img/arena/corner_top_right.png');
    game.load.image('vertical_wall_small', '/img/arena/vertical_wall_small.png');
    game.load.image('horizontal_wall_small', '/img/arena/horizontal_wall_small.png');
    game.load.image('bar', '/img/arena/bar.png');
    game.load.image('mana_bar', '/img/arena/mana_bar.png');
    game.load.image('health_bar', '/img/arena/health_bar.png');

    // block d inside
    game.load.image('bottom_courtyard_wall', '/img/buildings/block_d_inside/bottom_courtyard_wall.png');
    game.load.image('bottom_left_vertical_wall', '/img/buildings/block_d_inside/bottom_left_vertical_wall.png');
    game.load.image('bottom_outer_wall', '/img/buildings/block_d_inside/bottom_outer_wall.png');
    game.load.image('bottom_right_outer_wall', '/img/buildings/block_d_inside/bottom_right_outer_wall.png');
    game.load.image('bottom_right_vertical_wall', '/img/buildings/block_d_inside/bottom_right_vertical_wall.png');
    game.load.image('courtyard_outer_wall', '/img/buildings/block_d_inside/courtyard_outer_wall.png');
    game.load.image('left_outer_wall', '/img/buildings/block_d_inside/left_outer_wall.png');
    game.load.image('left_vertical_wall', '/img/buildings/block_d_inside/left_vertical_wall.png');
    game.load.image('right_vertical_wall', '/img/buildings/block_d_inside/right_vertical_wall.png');
    game.load.image('side_courtyard_wall', '/img/buildings/block_d_inside/side_courtyard_wall.png');
    game.load.image('top_left_inner_wall', '/img/buildings/block_d_inside/top_left_inner_wall.png');
    game.load.image('top_left_wall', '/img/buildings/block_d_inside/top_left_wall.png');
    game.load.image('top_wall', '/img/buildings/block_d_inside/top_wall.png');

    // skills
    game.load.spritesheet('fireball', 'img/skills/fireball.png', 62.5, 19);
    game.load.spritesheet('explosion', 'img/skills/explosion.png', 100, 72);
    game.load.image('pdm_head', 'img/skills/pdm_head.png');

    // quests
    game.load.image('pcparts', 'img/quests/1/PC.png');
    game.load.image('quest_case', 'img/quests/1/case.png');
    game.load.image('quest_cooler', 'img/quests/1/cooler.png');
    game.load.image('quest_cpu', 'img/quests/1/CPU.png');
    game.load.image('quest_hdd', 'img/quests/1/harddrive.png');
    game.load.image('quest_motherboard', 'img/quests/1/motherboard.png');
    game.load.image('quest_ram', 'img/quests/1/ram.png');
    game.load.image('quest_switch', 'img/quests/1/switch.png');
}
