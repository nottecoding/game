function searchQuest(questId, buildingId, modelId, xCord, yCord, model, doingQuest, finishedQuest) {
	this.questId = questId;
	this.buildingId = buildingId;
	this.modelId = modelId;
	this.xCord = xCord;
	this.yCord = yCord;
	this.model = model;
    this.model.scale.x = 2;
    this.model.scale.y = 2;
	this.doingQuest = doingQuest;
	this.finishedQuest = finishedQuest;

	// Getters and setters
    this.getQuestId = function getQuestId() {
        return this.questId;
    };

    this.getBuildingId = function getBuildingId() {
    	return this.buildingId;
    };

    this.getModelId = function getModelId() {
    	return this.modelId;
    };

    this.getModel = function getModel() {
    	return this.model;
    };

    this.getDoingQuest = function getDoingQuest() {
    	return this.doingQuest;
    };

    this.getFinishedQuest = function getFinishedQuest() {
    	return this.finishedQuest;
    };

    this.setDoingQuest = function setDoingQuest(doQuest) {
        this.doingQuest = doQuest;
    };

    this.setFinishedQuest = function setFinishedQuest(finished) {
        this.finishedQuest = finished;
    };
}