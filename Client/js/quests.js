function quest() {
	var possibleQuests = [],
		talking;
	var questObjects = [];
	var questKey = game.input.keyboard.addKey(Phaser.Keyboard.Q);

	this.userCanDoQuest = function userCanDoQuest() {
        talking = true;
	};

	this.setPossibleQuests = function setPossibleQuests(newQuest) {
		possibleQuests.push(newQuest);
	};

	this.setQuestObjects = function setQuestObjects(questObject) {
		questObjects.push(questObject);
	};

	this.getPossibleQuests = function getPossibleQuests() {
		return possibleQuests;
	};

	this.getQuestObjects = function getQuestObjects() {
		return questObjects;
	};

    this.checkForQuestObjects = function checkForQuestObjects() {
    	for (var i = 0; i < possibleQuests.length; i++) {
	    	for (var j = 0; j < questObjects.length; j++) {
	    		if(possibleQuests[i].getQuestId() == questObjects[j].getQuestId()) {
	    			questObjects[j].showQuestObjects(possibleQuests[i].getQuestId());
	    		};
	    	};
    	};
    };

	this.checkForQuest = function checkForQuest() {
		game.physics.arcade.overlap(quests, localPlayer.getModel(), questProgress, null, this);
		if(!game.physics.arcade.overlap(quests, localPlayer.getModel())) {
			talking = true;
		}

		game.physics.arcade.overlap(searchQuestsObjects, localPlayer.getModel(), pickupQuestObject, null, this);
	};

	function questProgress(player, quest) {
		if(talking == true) {
			for (var i = 0; i < possibleQuests.length; i++) {
				if(possibleQuests[i].getModel() == quest) {
					var completed = true;
					for (var j = 0; j < questObjects.length; j++) {
						if(possibleQuests[i].getQuestId() == questObjects[j].getQuestId()) {
				    		if (!questObjects[j].getObjectFound()) {
				    			completed = false;
				    		};
						};
			    	};
			    	if(completed) {
			    		possibleQuests[i].setFinishedQuest(true);
			    	};

	                if(!possibleQuests[i].getFinishedQuest()) {
	                	if(possibleQuests[i].getDoingQuest()) {
	                		chat.appendMessage("Hard time completing the quest?");
	                	} else {
	                		chat.appendMessage("Press Q to start this quest.");
	                		var currentQuest = possibleQuests[i];
	                		questKey.onDown.add(function() { startQuest(currentQuest); }, this);
	                	}
	                } else {
	                	chat.appendMessage("Congratulations on completing the quest!");

	                	// Add rewards
	                };
	            };
			};
			
			talking = false;
		}
	};

	function startQuest(quest) {
		if(quest.getDoingQuest() == false) {
			chat.appendMessage("You think you're up for it?");
			chat.appendMessage("Find all items to complete this quest.");
			quest.setDoingQuest(true);
			loadQuestObjects(quest);
		};
	};

	function loadQuestObjects(quest) {
		for (var i = 0; i < questObjects.length; i++) {
			questObjects[i].setVisible(true);
			questObjects[i].showQuestObjects(quest.getQuestId());
		};
	};

	function pickupQuestObject(player, object) {
		for (var i = 0; i < questObjects.length; i++) {
			if(object == questObjects[i].getModel()) {
				questObjects[i].setObjectFound(true);
				questObjects[i].setVisible(false);
			}
		};
		object.kill();
	};
}