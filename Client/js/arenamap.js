
function arenamap (arenaNumber) {
    buildingId = arenaNumber;
    localPlayer.setBuildingId(arenaNumber);
    background = game.add.sprite(0, 0, 'background_arena');
    var walls;
    var doors;
    var startX = 673;
    var startY = 170;
    var offsetXVerticalWalls = 145;
    var offsetYVerticalWalls = 420;
    var offsetXHorizontalWalls = 256;
    var offsetYHorizontalWalls = 227;
    var offsetXPlayers = offsetXVerticalWalls / 2;
    var offsetYPlayers = offsetYVerticalWalls + game.cache.getImage('vertical_wall_small').height / 2;
    var healthBarChallenger;
    var healthBarChallengerFilling;
    var manaBarChallenger;
    var manaBarChallengerFilling;
    var healthBarChallenged;
    var healthBarChallengedFilling;
    var manaBarChallenged;
    var manaBarChallengedFilling;
    var offsetBars = 30;
    var fillingOffsetX = 51;
    var fillingOffsetY = 32;
    var challenger;
    var challenged;

    this.init = function init() {
        $('#chat').css({'display':'none'});
        walls = [];
        doors = [];
        var arena_floor = game.add.sprite(startX, startY, 'arena_floor');
        var odisee_logo = game.add.sprite(startX + game.cache.getImage('arena_floor').width / 2 - game.cache.getImage('odisee_logo').width / 2, startY + game.cache.getImage('arena_floor').height / 2 - game.cache.getImage('odisee_logo').height / 2, 'odisee_logo');
        this.addWalls();
        this.addSkillBars();

        game.world.bringToTop(arena);
        this.addDoors();

        if(localPlayer.getIsChallenger()) {
            localPlayer.changeBuilding(arenaNumber, startX + game.cache.getImage('arena_floor').width - offsetXPlayers, startY + offsetYPlayers);
            challenger = localPlayer;
            challenged = challenge.opponent;
        }
        else {
            localPlayer.changeBuilding(arenaNumber, startX + offsetXPlayers, startY + offsetYPlayers);
            challenged = localPlayer;
            challenger = challenge.opponent;
        }
    };

    this.addSkillBars = function addSkillBars() {
        healthBarChallengedFilling = arena.create(startX - game.cache.getImage('bar').width + fillingOffsetX - offsetBars, startY + fillingOffsetY, 'health_bar');
        healthBarChallenged = arena.create(startX - game.cache.getImage('bar').width - offsetBars, startY, 'bar');
        manaBarChallengedFilling = arena.create(startX - game.cache.getImage('bar').width + fillingOffsetX - offsetBars, startY + game.cache.getImage('bar').height + fillingOffsetY + 30, 'mana_bar');
        manaBarChallenged = arena.create(startX - game.cache.getImage('bar').width - offsetBars, startY + game.cache.getImage('bar').height + 30, 'bar');
        healthBarChallengerFilling = arena.create(startX + game.cache.getImage('arena_floor').width + fillingOffsetX + offsetBars, startY + fillingOffsetY, 'health_bar');
        healthBarChallenger = arena.create(startX + game.cache.getImage('arena_floor').width + offsetBars, startY, 'bar');
        manaBarChallengerFilling = arena.create(startX + game.cache.getImage('arena_floor').width + offsetBars + fillingOffsetX, startY + game.cache.getImage('bar').height + fillingOffsetY + 30, 'mana_bar');
        manaBarChallenger = arena.create(startX + game.cache.getImage('arena_floor').width + offsetBars, startY + game.cache.getImage('bar').height + 30, 'bar');
    };

    this.addMana = function addMana(amount) {
        if(challenger.getMana() < 100) {
            challenger.setMana(challenger.getMana() + amount);
        }
        if(challenged.getMana() < 100) {
            challenged.setMana(challenged.getMana() + amount);
        }
    };

    this.addWalls = function addWalls() {
        var horizontal_wall_top = arena.create(startX + game.cache.getImage('corner_top_left').width, startY, 'horizontal_wall_top');
        horizontal_wall_top.body.immovable = true;

        var vertical_wall_left = arena.create(startX, startY + game.cache.getImage('corner_top_left').height - 7, 'vertical_wall_left');
        vertical_wall_left.body.immovable = true;

        var vertical_wall_right = arena.create(startX + game.cache.getImage('arena_floor').width - game.cache.getImage('vertical_wall_right').width, startY + game.cache.getImage('corner_top_right').height - 5, 'vertical_wall_right');
        vertical_wall_right.body.immovable = true;

        var horizontal_wall_bottom = arena.create(startX - 1, startY + game.cache.getImage('arena_floor').height - game.cache.getImage('horizontal_wall_bottom').height, 'horizontal_wall_bottom');
        horizontal_wall_bottom.body.immovable = true;

        var corner_top_left = arena.create(startX, startY, 'corner_top_left');
        corner_top_left.body.immovable = true;

        var corner_top_right = arena.create(startX + game.cache.getImage('arena_floor').width - game.cache.getImage('corner_top_right').width, startY, 'corner_top_right');
        corner_top_right.body.immovable = true;

        var vertical_wall_small_left = arena.create(startX + offsetXVerticalWalls, startY + offsetYVerticalWalls, 'vertical_wall_small');
        vertical_wall_small_left.body.immovable = true;

        var vertical_wall_small_right = arena.create(startX + game.cache.getImage('arena_floor').width - offsetXVerticalWalls - game.cache.getImage('vertical_wall_small').width, startY + offsetYVerticalWalls, 'vertical_wall_small');
        vertical_wall_small_right.body.immovable = true;

        var horizontal_wall_small_left_top = arena.create(startX + offsetXHorizontalWalls, startY + offsetYHorizontalWalls, 'horizontal_wall_small');
        horizontal_wall_small_left_top.body.immovable = true;

        var horizontal_wall_small_right_top = arena.create(startX + game.cache.getImage('arena_floor').width - offsetXHorizontalWalls - game.cache.getImage('horizontal_wall_small').width, startY + offsetYHorizontalWalls, 'horizontal_wall_small');
        horizontal_wall_small_right_top.body.immovable = true;

        var horizontal_wall_small_left_bottom = arena.create(startX + offsetXHorizontalWalls, startY + game.cache.getImage('arena_floor').height - offsetYHorizontalWalls - game.cache.getImage('horizontal_wall_small').height, 'horizontal_wall_small');
        horizontal_wall_small_left_bottom.body.immovable = true;

        var horizontal_wall_small_right_bottom = arena.create(startX + game.cache.getImage('arena_floor').width - offsetXHorizontalWalls - game.cache.getImage('horizontal_wall_small').width , startY + game.cache.getImage('arena_floor').height - offsetYHorizontalWalls - game.cache.getImage('horizontal_wall_small').height, 'horizontal_wall_small');
        horizontal_wall_small_right_bottom.body.immovable = true;

    };

    this.addDoors = function addDoors() {
        doors.push(new entrance('door_d', startX + game.cache.getImage('arena_floor').width / 2, startY + game.cache.getImage('horizontal_wall_top').height - 20, 'mainmap', 'brickdoor'));

        doors.forEach(function(door){
            var entrance = entrances.create(door.getX(), door.getY(), door.getModel());
            entrance.body.immovable = true;
            game.world.bringToTop(entrances);
        });

    };

    this.checkEntranceCollision = function checkEntranceCollision() {
        game.physics.arcade.overlap(localPlayer.getModel(), entrances, enterBuilding, null, this);
    };

    function enterBuilding(player, entrance) {
        if (cursors.up.isDown) {
            doors.forEach(function (door) {
                game.time.events.events = [];
                $('#chat').css({'display':'block'});
                localPlayer.setIsChallenger(false);
                challenge.setArenaOpen(false);
                localPlayer.getPlayerSettings().unsetSkillKeys();
                loadMap(maps[1], 0);
            });
        }
    }

    this.setHealthbars = function setHealthbars() {
        if(challenger.getHealth()/100 > 0) {
            healthBarChallengerFilling.scale.set(challenger.getHealth()/100 , 1 );
        }
        else {
            healthBarChallengerFilling.scale.set(0 , 1 );
        }
        if(challenged.getHealth()/100 > 0) {
            healthBarChallengedFilling.scale.set(challenged.getHealth()/100 , 1 );
        }
        else {
            healthBarChallengedFilling.scale.set(0 , 1 );
        }
    };
    this.setManabars = function setManabars() {
        manaBarChallengerFilling.scale.set(challenger.getMana()/100 , 1 );
        manaBarChallengedFilling.scale.set(challenged.getMana()/100 , 1 );
    };

    this.getChallenger = function getChallenger() {
        return challenger;
    };

    this.getChallenged = function getChallenged() {
        return challenged;
    };
}