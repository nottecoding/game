function mainmap () {
    buildingId = 1;
    background = game.add.sprite(0, 0, 'background');

    var doors;

    this.init = function init() {
        localPlayer.changeBuilding(1, 470, 660);
        doors = [];
        this.addBuildings();
        this.addFlowersAndBushes();
        this.addTrees();
        this.addDoors();
        this.addMiscellaneous();
        game.world.bringToTop(flora);
    };

    this.addBuildings = function addBuildings() {
        var block_b = buildings.create(55, 587, 'block_b');
        block_b.body.immovable = true;
        block_b.body.scale = 0.5;
        this.createBlockD();
        var block_e = buildings.create(992, -3, 'block_e');
        block_e.body.immovable = true;
        var block_m = buildings.create(1743, 133, 'block_m');
        block_m.body.immovable = true;
        var block_l = buildings.create(2047, 133, 'block_m');
        block_l.body.immovable = true;
    };

    this.createBlockD = function createBlockD() {
        var block_d = buildings.create(0, 208, 'block_d_left_wing');
        block_d.body.immovable = true;
        block_d = buildings.create(892, 28, 'block_d_right_wing');
        block_d.body.immovable = true;
        block_d = buildings.create(405, 410, 'block_d_tower');
        block_d.body.immovable = true;
        block_d = buildings.create(759, 412, 'block_d_bottom_right');
        block_d.body.immovable = true;
    };

    this.addTrees = function addTrees() {
        this.addTopLeftTrees();
        this.addRightGrassfieldTrees();
        this.addLeftGrassfieldSmallTrees();
        this.addMiddleGrassfieldTrees();
    };

    this.addTopLeftTrees = function addTopLeftTrees() {
        for (var i = 930; i > -65; i -= 65) {
            var tree = flora.create(i, -30, 'tree');
            tree.body.immovable = true;
        }
    };

    this.addRightGrassfieldTrees = function addRightGrassfieldTrees() {
        var tree = flora.create(1765, 1000, 'tree');
        tree.body.immovable = true;
        tree = flora.create(1890, 1000, 'tree');
        tree.body.immovable = true;
        tree = flora.create(1800, 1035, 'tree');
        tree.body.immovable = true;
        tree = flora.create(1855, 1035, 'tree');
        tree.body.immovable = true;
        tree = flora.create(1780, 820, 'tree_small');
        tree.body.immovable = true;
        tree = flora.create(1900, 820, 'tree_small');
        tree.body.immovable = true;

        // bottom trees
        for (var i = 1300; i < 2200; i += 70) {
            for (var j = 0; j < 200 ; j += 50) {
                var tree = flora.create(i, 1210+j, 'tree');
                tree.body.immovable = true;
            }
        }
    };

    this.addLeftGrassfieldSmallTrees = function addLeftGrassfieldSmallTrees() {
        for (var i = 535; i < 950; i += 100) {
            var tree = flora.create(i, 750, 'tree_small');
            tree.body.immovable = true;
        }

        for (var i = 450; i <= 1000; i += 110) {
            var tree = flora.create(i, 1230, 'tree_small');
            tree.body.immovable = true;
        }

        var tree = flora.create(535, 810, 'tree_small');
        tree.body.immovable = true;
        tree = flora.create(450, 810, 'tree_small');
        tree.body.immovable = true;

        // bottom left trees
        for (var i = 485; i < 540; i += 40) {
            var tree = flora.create(i, 1385, 'tree_small');
            tree.body.immovable = true;
        }

        // bottom left 2nd row trees
        for (var i = 790; i < 1050; i += 70) {
            var tree = flora.create(i, 1385, 'tree_small');
            tree.body.immovable = true;
        }
    };

    this.addMiddleGrassfieldTrees = function addMiddleGrassfieldSmallTrees() {
        for (var i = 1070; i < 1600; i += 120) {
            var tree = flora.create(i, 750, 'tree_small');
            tree.body.immovable = true;
        }
        for (var i = 830; i < 1050; i += 150) {
            var tree = flora.create(1630, i, 'tree_small');
            tree.body.immovable = true;
        }
        for (var i = 1400; i < 1600; i += 150) {
            var tree = flora.create(i, 1080, 'tree_small');
            tree.body.immovable = true;
        }

        var tree = flora.create(1255, 920, 'tree');
        tree.body.immovable = true;
        tree = flora.create(1325, 880, 'tree');
        tree.body.immovable = true;
        tree = flora.create(1355, 800, 'tree');
        tree.body.immovable = true;
        tree = flora.create(1435, 780, 'tree');
        tree.body.immovable = true;

        // bottom trees
        var tree = flora.create(1030, 1350, 'tree');
        tree.body.immovable = true;
        tree = flora.create(1100, 1330, 'tree');
        tree.body.immovable = true;
        tree = flora.create(1140, 1260, 'tree');
        tree.body.immovable = true;
    };

    this.addFlowersAndBushes = function addFlowersAndBushes() {
        // left grassfield flowers
        for (var i = 480; i < 950; i += Math.floor((Math.random() * 100) + 50)) {
            var flower = flowers.create(i, 850+Math.floor((Math.random() * 300) + 50), 'flower');
            flower.body.immovable = true;
        }

        // middle grassfield flowers
        for (var i = 1200; i < 1600; i += Math.floor((Math.random() * 100) + 50)) {
            var flower = flowers.create(i, 800+Math.floor((Math.random() * 250) + 50), 'flower');
            flower.body.immovable = true;
        }

        // bushes right grass field
        for (var i = 780; i < 1020; i += 40) {
            var bush = flora.create(1745, i, 'bush');
            bush.body.immovable = true;
        }
        var bush = flora.create(1775, 780, 'bush');
        bush.body.immovable = true;

        for (var i = 780; i < 1020; i += 40) {
            var bush = flora.create(1937, i, 'bush');
            bush.body.immovable = true;
        }
        var bush = flora.create(1907, 780, 'bush');
        bush.body.immovable = true;

        // bushes and flowers bottom left
        for (var i = 445; i < 580; i += 30) {
            var bush = flora.create(i, 1350, 'bush');
            bush.body.immovable = true;
        }

        var bush = flora.create(445, 1390, 'bush');
        bush.body.immovable = true;
        var bush = flora.create(565, 1390, 'bush');
        bush.body.immovable = true;

        // middle bottom bushes
        for (var i = 755; i < 1050; i += 30) {
            var bush = flora.create(i, 1350, 'bush');
            bush.body.immovable = true;
        }

        var bush = flora.create(755, 1390, 'bush');
        bush.body.immovable = true;
    };

    this.addDoors = function addDoors() {
        doors.push(new entrance('door_d', 463, 635, 'block_d', 'brickdoor'));
        doors.push(new entrance('door_e', 995, 567, 'block_e', 'slidingdoor'));
        doors.push(new entrance('door_m', 1799, 570, 'block_m', 'bigSlidingdoor'));
        doors.push(new entrance('door_l', 2103, 570, 'block_l', 'bigSlidingdoor'));

        doors.forEach(function(door){
            var entrance = entrances.create(door.getX(), door.getY(), door.getModel());
            entrance.body.immovable = true;
            if (door.getName() == 'door_d') {
                entrance.scale.y = 2;
            }
        });
    };

    this.addMiscellaneous = function addMiscellaneous() {
        var water = miscellaneous.create(2200, 752, 'water_mainmap');
        water.body.immovable = true;
        water.scale.y = 1.01;
        var fountain = miscellaneous.create(1035, 1131, 'fountain');
        fountain.body.immovable = true;
        fountain.animations.add('spray', [0, 1, 2, 3, 4], 12, true);
        fountain.animations.play('spray');
        for (var i = 780; i < 1300; i += 137) {
            for (var j = 0; j <= 120; j += 120) {
                var windTurbine = miscellaneous.create(2285+j, i, 'wind turbine');
                windTurbine.body.immovable = true;
                windTurbine.animations.add('windy', [0, 1, 2, 3, 4], Math.floor((Math.random() * 10) + 10), true);
                windTurbine.animations.play('windy');
            }
        }
    };

    this.checkEntranceCollision = function checkEntranceCollision() {
        game.physics.arcade.overlap(localPlayer.getModel(), entrances, enterBuilding, null, this);
    };

    function enterBuilding(player, entrance) {
        if (cursors.up.isDown) {
            doors.forEach(function (door) {
                if (entrance.position.x == door.getX() && entrance.position.y == door.getY()) {
                    loadMap(door.getLeadsTo());
                }
            });
        }
    }
}