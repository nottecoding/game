//TODO: ruben xp
function player (userId, name, health, mana, level, skillpower, skills, modelId, classId, xCord, yCord, model, buildingId){
    this.userId = userId;
    this.name = name;
    this.health = health;
    this.mana = mana;
    this.level = level;
    this.skillpower = skillpower;
    this.skills = skills;
    this.modelId = modelId;
    this.classId = classId;
    this.xCord = xCord;
    this.yCord = yCord;
    this.model = model;
    this.model.scale.x = 1.5;
    this.model.scale.y = 1.5;

    var frame = 0;
    var direction = '';
    this.buildingId = buildingId;
    var playerSettings;
    var isChallenger = false;
    var skillKeyReleased = true;
    //TODO: ruben
    var defense = false;

    // Getters and setters
    this.getUserId = function getUserId() {
        return this.userId;
    };

    this.getName = function getName() {
        return this.name;
    };

    this.getHealth = function getHealth() {
        return this.health;
    };

    this.getMana = function getMana() {
        return this.mana;
    };

    this.getLevel = function getLevel() {
        return this.level;
    };

    this.getSkillpower = function getSkillpower() {
        return this.skillpower
    };

    this.getSkills = function getSkills() {
        return this.skills;
    };

    this.getModelId = function getModelId() {
        return this.modelId
    };

    this.getClassId = function getClassId() {
        return this.classId
    };

    this.getUserId = function getUserId() {
        return this.userId
    };

    this.getX = function getX() {
        return this.xCord;
    };

    this.getY = function getY() {
        return this.yCord;
    };

    this.getModel = function getModel() {
        return this.model;
    };

    this.getDirection = function getDirection() {
        return direction;
    };

    this.getBuildingId = function getBuildingId () {
        return this.buildingId;
    };

    this.getIsChallenger = function  getIsChallenger() {
        return isChallenger;
    };

    this.getPlayerSettings = function getPlayerSettings() {
        return playerSettings;
    };

    this.getFrame = function getFrame() {
        return frame;
    }

    this.getDefense = function getDefense() {
        return defense;
    }

    this.setMana = function setMana(newMana) {
        this.mana = newMana;
    };

    this.setLevel = function setLevel(newLevel) {
        this.level = newLevel;
    };

    this.setSkillpower = function setSkillpower(newSkillpower) {
        this.skillpower = newSkillpower;
    };

    this.setX = function setX(newX) {
        this.xCord = newX;
    };

    this.setY = function setY(newY) {
        this.yCord = newY;
    };

    this.setDirection = function setDirection(newDirection) {
        direction = newDirection;
    };

    this.setBuildingId = function setBuildingId(newBuildingId) {
        this.buildingId = newBuildingId;
    };

    this.setHealth = function setHealth(newHealth) {
        this.health = newHealth;
    };

    this.setIsChallenger = function setIsChallenger(newIsChallenger) {
        isChallenger = newIsChallenger;
    };

    this.setFrame = function setFrame(newFrame) {
        frame = newFrame;
    }

    this.setDefense = function setDefense(newDefense) {
        defense = newDefense;
    }

    this.init = function init() {
        this.physics();
        this.animations();
        this.model.position.x = this.getX();
        this.model.position.y = this.getY();
    };

    this.physics = function physics() {
        game.physics.enable(this.model, Phaser.Physics.ARCADE);
        this.model.body.collideWorldBounds = true;
    };

    this.animations = function animations() {
        this.model.animations.add('up', [10, 9, 11], 11, true);
        this.model.animations.add('down', [1, 0, 2], 11, true);
        this.model.animations.add('left', [4, 3, 5], 11, true);
        this.model.animations.add('right', [7, 6, 8], 11, true);
    };

    this.controls = function controls() {
        var playerModel = this.model;
        playerModel.body.velocity.x = 0;
        playerModel.body.velocity.y = 0;

        if (cursors.left.isDown) {
            frame = 4;
            direction = 'left';
            moveLeft();
        }
        else if (cursors.right.isDown) {
            frame = 7;
            direction = 'right';
            moveRight();
        }
        else if (cursors.up.isDown) {
            frame = 10;
            direction = 'up';
            moveUp();
        }
        else if (cursors.down.isDown) {
            frame = 0;
            direction = 'down';
            moveDown();
        }
        else {
            standStill(frame);
        }

        function moveLeft() {
            playerModel.body.velocity.x = -150;
            playAnimationSetAndEmitCords();
        }

        function moveRight() {
            playerModel.body.velocity.x = 150;
            playAnimationSetAndEmitCords();
        }

        function moveUp() {
            playerModel.body.velocity.y = -150;
            playAnimationSetAndEmitCords();
        }

        function moveDown() {
            playerModel.body.velocity.y = 150;
            playAnimationSetAndEmitCords();
        }

        function standStill(frame) {
            playerModel.animations.stop();
            localPlayer.setCoordinates();
            playerModel.frame = frame;

            socket.emit("stop player", {
                userId: userId,
                frame: frame,
                direction: direction,
                x: localPlayer.getX(),
                y: localPlayer.getY()
            });
        }

        function playAnimationSetAndEmitCords() {
            localPlayer.setCoordinates();
            playerModel.animations.play(direction);

            socket.emit("move player", {
                userId: userId,
                x: localPlayer.getX(),
                y: localPlayer.getY(),
                frame: frame,
                direction: direction
            });
        }
    };

    this.setCoordinates = function setCoordinates() {
        this.setX(this.model.position.x);
        this.setY(this.model.position.y);
    }

    this.animateMovement = function animateMovement(playerStopped) {
        if (playerStopped == undefined) {
            this.model.animations.play(direction);
        } else {
            this.model.animations.stop();
            this.model.frame = frame;
        }
    };

    this.initialiseForArena = function initialiseForArena() {
        if (skills != undefined) {
            playerSettings = new settings();
            this.setSettings();
        }
    };

    this.setSettings = function setSettings() {
        playerSettings.bindSkillToKey(0, this.skills[0]);
        playerSettings.bindSkillToKey(1, this.skills[1]);
        playerSettings.bindSkillToKey(2, this.skills[2]);
        playerSettings.bindSkillToKey(3, this.skills[3]);
    };

    this.checkSkillKeys = function checkSkillKeys() {
        for (var i = 0; i < playerSettings.getSkillKeys().length; i++) {
            if(playerSettings.getSkillKeys()[i].getKey().isDown && skillKeyReleased) {
                doSkill(playerSettings.getSkillKeys()[i].getSkill());
            }
        }
        game.input.keyboard.onUpCallback = function (e) {
            if (e.keyCode == Phaser.Keyboard.E || e.keyCode == Phaser.Keyboard.R || e.keyCode == Phaser.Keyboard.T || e.keyCode == Phaser.Keyboard.Y) {
                skillKeyReleased = true;
            }
        };
    };

    function doSkill(skill) {
        skillKeyReleased = false;
        if (localPlayer.getMana() - skill.mana > 0) {
            var power = ( 1 + localPlayer.getLevel() / 10) * skill.skillpower;
            if (skill.getType() == "ranged") {
                animations.animateRangedAttack(localPlayer.getX(), localPlayer.getY(), localPlayer.getDirection(), skill, localPlayer.getLevel(), true);
            }
            else if (skill.getType() == "melee") {
                animations.animateMeleeAttack(localPlayer.getX(), localPlayer.getY(), localPlayer.getDirection(), skill, localPlayer.getLevel(), true);
            }
            else {
                animations.animateDefense(localPlayer.getX(), localPlayer.getY(), localPlayer.getDirection(), skill, localPlayer.getLevel(), true);
            }

            localPlayer.setMana(localPlayer.getMana() - skill.mana);

            socket.emit("player skill", {
                sendToUserId: challenge.opponent.getUserId(),
                userId: localPlayer.getUserId(),
                mana: localPlayer.getMana(),
                x: localPlayer.getX(),
                y: localPlayer.getY(),
                direction: direction,
                skill: skill,
                //TODO ruben
                characterLevel: localPlayer.getLevel()
            });

            console.log("player skill: " + skill.getName());
        }
    }

    this.changeBuilding = function changeBuilding(buildingId, x, y) {
        this.model.body.position.x = x;
        this.model.body.position.y = y;

        this.setBuildingId(buildingId);

        socket.emit("player building changed", {userId: localPlayer.getUserId(), buildingId: buildingId});
    };
}