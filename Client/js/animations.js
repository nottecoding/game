function animations() {
    this.init = function init() {
        enemySkillGroup.createMultiple(100, 'fireball');
        enemySkillGroup.physicsBodyType = Phaser.Physics.ARCADE;
        enemySkillGroup.setAll('anchor.x', 0.5);
        enemySkillGroup.setAll('anchor.y', 0.5);

        localSkillGroup.createMultiple(100, 'fireball');
        localSkillGroup.physicsBodyType = Phaser.Physics.ARCADE;
        localSkillGroup.setAll('anchor.x', 0.5);
        localSkillGroup.setAll('anchor.y', 0.5);

        explosions.createMultiple(100, 'explosion');
        explosions.physicsBodyType = Phaser.Physics.ARCADE;
        explosions.setAll('anchor.x', 0.5);
        explosions.setAll('anchor.y', 0.5);

    }

    this.animateRangedAttack = function animateRangedAttack(x, y, direction, skill, characterLevel, isLocalPlayer) {
        var power = (1 + characterLevel / 10) * skill.skillpower;
        if(isLocalPlayer) {
            var fireball = localSkillGroup.getFirstExists(false);
        }
        else {
            var fireball = enemySkillGroup.getFirstExists(false);
        }

        if (fireball) {
            fireball.animations.add('burn', [1, 2, 1, 2], 15, true);
            fireball.animations.play('burn');
            fireball.reset(x + localPlayer.getModel().getBounds().width / 2, y + localPlayer.getModel().getBounds().height / 2);
            fireball.power = power;

            if (direction == 'left') {
                fireball.rotation = 3.14;
                fireball.body.velocity.x = -400;
            } else if (direction == 'right') {
                fireball.rotation = 0;
                fireball.body.velocity.x = +400;
            } else if (direction == 'up') {
                fireball.rotation = 4.71;
                fireball.body.velocity.y = -400;
            } else if (direction == 'down') {
                fireball.rotation = 1.57;
                fireball.body.velocity.y = +400;
            }
        }

        // TODO: checken op collision
    };

    this.animateMeleeAttack = function animateMeleeAttack(x, y, direction, skill, characterLevel, isLocalPlayer) {
        var power = (1 + characterLevel / 10) * skill.skillpower;
        console.log("melee animatie uitgevoerd");
        if(!isLocalPlayer) {
            if(game.physics.arcade.distanceBetween(map.getChallenger().model, map.getChallenged().model) < localPlayer.getModel().getBounds().width) {
                if(localPlayer.getDefense()) {
                    localPlayer.setDefense(false);
                }
                else {
                    localPlayer.setHealth(localPlayer.getHealth() - power);
                    socket.emit("player got hit", {
                        sendToUserId: challenge.opponent.getUserId(),
                        userId: localPlayer.getUserId(),
                        health: localPlayer.getHealth()
                    });
                }
            }
        }
    };

    this.animateDefense = function animateDefense(x, y, direction, skill, characterLevel, isLocalPlayer) {
        console.log("defense animatie uitgevoerd");
        if(isLocalPlayer) {
            localPlayer.setDefense(true);
        }
    };
}
