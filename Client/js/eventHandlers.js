function eventHandlers() {
    // Socket connection successful
    socket.on("connect", onSocketConnected);

    // Socket disconnection
    socket.on("disconnect", onSocketDisconnect);

    socket.on("request skills", onRequestSkills);

    socket.on("response character data", onRequestPlayerData);

    // New player message received
    socket.on("new player", onNewPlayer);

    // Player move message received
    socket.on("move player", onMovePlayer);

    // Player move message received
    socket.on("stop player", onStopPlayer);

    // Player removed message received
    socket.on("remove player", onRemovePlayer);

    // Remote player changed building
    socket.on("player building changed", onBuildingChanged)

    // Player removed message received
    socket.on("receive message", onMessageReceived);

    // Player did skill message received
    socket.on("player skill", onPlayerSkill);

    socket.on("request player duel", onRequestDuel);

    socket.on("reply challenge", onReplyDuel);

    socket.on("add mana", onAddMana);

    //TODO ruben
    socket.on("player got hit", onPlayerHit);

    socket.on("response quest data", onResponseQuestData);

    socket.on("response questObject data", onResponseQuestObjectData);
}

// Socket connected
function onSocketConnected() {
    console.log("Connected to socket server");

    if (localPlayer != undefined) {
        socket.emit("new player", {
            userId: localPlayer.getUserId(),
            name: localPlayer.getName(),
            health: localPlayer.getHealth(),
            mana: localPlayer.getMana(),
            level: localPlayer.getLevel(),
            skillpower: localPlayer.getSkillpower(),
            skills: localPlayer.getSkills(),
            modelId: localPlayer.getModelId(),
            classId: localPlayer.getClassId(),
            x: localPlayer.getX(),
            y: localPlayer.getY(),
            buildingId: localPlayer.getBuildingId()
        });
    }
};

// Socket disconnected
function onSocketDisconnect() {
    console.log("Disconnected from socket server");
};

function onRequestSkills(data) {
    if (skills == undefined) {
        skills = [];

        for (var i in data) {
            skills.push(new skill(data[i].id, data[i].skillname, data[i].description, data[i].skillpower, data[i].skillMana, data[i].type, data[i].skilllevel));
        }

        if (localPlayer != undefined) {
            localPlayer.setSettings();
        }

        console.log("skills retrieved");
    }
}

function onRequestPlayerData(data) {
    if (localPlayer == undefined) {
        console.log(data);
        localPlayer = new player(data.userId, data.name, data.health, data.mana, data.level, data.playerSkillpower, skills, data.modelid, data.classid, data.x, data.y, game.add.sprite(data.x, data.y, models[data.modelid]), data.buildingId);
        localPlayer.init();

        // initialise map when the player is loaded
        if (localPlayer.getBuildingId() > 10) {
            challenge.setArenaOpen(false);
            localPlayer.setIsChallenger(false);
            localPlayer.setBuildingId(1);
        }

        socket.emit("new player", {
            userId: localPlayer.getUserId(),
            name: localPlayer.getName(),
            health: localPlayer.getHealth(),
            mana: localPlayer.getMana(),
            level: localPlayer.getLevel(),
            skillpower: localPlayer.getSkillpower(),
            skills: localPlayer.getSkills(),
            modelId: localPlayer.getModelId(),
            classId: localPlayer.getClassId(),
            x: localPlayer.getX(),
            y: localPlayer.getY(),
            buildingId: localPlayer.getBuildingId()
        });

        loadMap(maps[localPlayer.getBuildingId()]);
        console.log("player loaded");
    }
}

// New player
function onNewPlayer(data) {
    if (data.userId != localPlayer.getUserId()) {
        console.log("new player");

        var newPlayer = new player(data.userId, data.name, data.health, data.mana, data.level, data.skillpower, data.skills, data.modelId, data.classId, data.x, data.y, game.add.sprite(data.x, data.y, models[data.modelId]), data.buildingId);
        newPlayer.animations();

        remotePlayers.addToAllRemotePlayers(newPlayer);

        game.world.sendToBack(newPlayer.getModel());

        chat.appendMessage("New player connected: " + data.name);
    }
};

// Remove player
function onRemovePlayer(data) {
    remotePlayers.removeFromAllRemotePlayers(data.userId);

    chat.appendMessage('Player disconnected: ' + data.name);
};

// Move player
function onMovePlayer(data) {
    remotePlayers.movePlayer(data.userId, data.x, data.y, data.frame, data.direction);
};

function onStopPlayer(data) {
    remotePlayers.stopPlayer(data.userId, data.x, data.y, data.frame, data.direction);
}

// Paste message in chatroom
function onMessageReceived(data) {
    chat.appendMessage('[' + data.time + '] ' + data.name + ': ' + data.chatMessage);
}

function onBuildingChanged(data) {
    remotePlayers.playerBuildingChanged(data.userId, data.buildingId);
};

//TODO ruben skill
function onPlayerSkill(data) {
    if (data.sendToUserId == localPlayer.getUserId()) {
        if (map instanceof arenamap == true) {
            if (data.userId == map.getChallenger().userId) {
                map.getChallenger().mana = data.mana;
            }
            else if (data.userId == map.getChallenged().userId) {
                map.getChallenged().mana = data.mana;
            }

            if (data.skill.type == "ranged") {
                animations.animateRangedAttack(data.x, data.y, data.direction, data.skill, data.characterLevel, false);
            }
            else if (data.skill.type == "melee") {
                animations.animateMeleeAttack(data.x, data.y, data.direction, data.skill, data.characterLevel, false);
            }
            else if (data.skill.type == "defense") {
                animations.animateDefense(data.x, data.y, data.direction, data.skill, data.characterLevel, false);
            }
        }
    }
}

function onRequestDuel(data) {
    if (data.challengedId == localPlayer.getUserId()) {
        var challenger = remotePlayers.findPlayerById(data.challengerId);

        chat.appendMessage(challenger.getName() + " has challenged you in a duel");

        if (confirm(challenger.getName() + " has challenged you in a duel, do you wish to accept or not?") == true) {
            socket.emit("reply challenge", {challengerId: data.challengerId, answer: true});
            challenge.opponent = challenger;
            localPlayer.initialiseForArena();
            challenge.setArenaOpen(true);
            localPlayer.setIsChallenger(true);
            localPlayer.setBuildingId(data.challengerId + 10);
        } else {
            socket.emit("reply challenge", {challengerId: data.challengerId, answer: false});
        }
    }
}

function onReplyDuel(data) {
    if(data.challengerId == localPlayer.getUserId()) {
        if(data.answer) {
            localPlayer.initialiseForArena();
            challenge.setArenaOpen(true);
            localPlayer.setIsChallenger(false);
            localPlayer.setBuildingId(data.challengerId + 10);
        } else {
            chat.appendMessage("Your challenge request was declined");
        }
    }
}

function onAddMana(data) {
    if (map instanceof arenamap == true) {
        map.addMana(data.amount);
    }
}

function onPlayerHit(data) {
    if (data.sendToUserId == localPlayer.getUserId()) {
        if (map instanceof arenamap == true) {
            if (data.userId == map.getChallenger().userId) {
                map.getChallenger().health = data.health;
            }
            else if (data.userId == map.getChallenged().userId) {
                map.getChallenged().health = data.health;
            }
        }
    }
}

function onResponseQuestData(data) {
    for (var i = 0; i < data.length; i++) {
        quest.setPossibleQuests(new searchQuest(data[i].questId, data[i].buildingId, data[i].modelId, data[i].xCord, data[i].yCord, game.add.sprite(data[i].xCord, data[i].yCord, models[data[i].modelId]), data[i].doingQuest, data[i].finishedQuest));
    };
}

function onResponseQuestObjectData(data) {
    for (var i = 0; i < data.length; i++) {
        quest.setQuestObjects(new questObjects(data[i].objectId, data[i].questId, data[i].objectFound, data[i].xCord, data[i].yCord, data[i].buildingId, game.add.sprite(data[i].xCord, data[i].yCord, data[i].modelName), data[i].visible, data[i].loaded));
    };
}