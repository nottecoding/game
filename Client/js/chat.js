function chat () {
    var chatKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
    var chatScreenOpen = false;
    var enterReleased = true;

    this.getChatScreenOpen = function getChatScreenOpen() {
        return chatScreenOpen;
    }

    this.checkChatKey = function checkChatKey() {
        if (chatKey.isDown && enterReleased) {
            enterReleased = false;
            if (chatScreenOpen) {
                this.sendChatMessage();
            }
            else {
                this.openChatScreen();
            }
        }

        game.input.keyboard.onUpCallback = function( e ){
            if(e.keyCode == Phaser.Keyboard.ENTER){
                enterReleased = true;
            }
        };
    }

    this.sendChatMessage = function sendChatMessage(){
        if ($('#chatInput').val() != '') {
            socket.emit("send message", {name: localPlayer.getName() ,msg: $('#chatInput').val()});
            $('#chatarea').append('<p>' + $('#chatInput').val() + '</p>');
            $('#chatInput').val('');
            $("#chatarea").scrollTop($("#chatarea")[0].scrollHeight);
        }

        chatScreenOpen = false;
        $('#chatInput').css('display', 'none');
    }

    this.openChatScreen = function openChatScreen(){
        chatScreenOpen = true;
        $('#chatInput').css('display', 'block');
        $('#chatInput').focus();
    }

    this.appendMessage = function appendMessage(message) {
        $('#chatarea').append('<p>' + message + '</p>');
        $("#chatarea").scrollTop($("#chatarea")[0].scrollHeight);
        $("#chatarea").animate({ scrollTop: $("#chatarea")[0].scrollHeight}, 1000);
    }

    this.broadcastMessage = function broadcastMessage(message) {
        socket.emit("send message", {name: localPlayer.getName() ,msg: message});

    }
}
