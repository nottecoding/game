var game = new Phaser.Game(2560, 1440, Phaser.AUTO, 'game-canvas', { preload: preload, create: create, update: update });

var localPlayer,
    remotePlayers,
    remotePlayersCollision,
    skills,
    explosions,
    enemySkillGroup,
    localSkillGroup,
    cursors,
    buildings,
    wallsGroup,
    flora,
    flowers,
    entrances,
    miscellaneous,
    socket,
    chat,
    map,
    background,
    arena,
    animations,
    challenge,
    buildingId,
    quests,
    quest,
    searchQuestsObjects;

var maps = {1: 'mainmap', 2:'arenamap', 3: 'block_d'};
var models = {1:'player_model1', 2:'player_model2', 3:'player_model3', 4:'player_model4'};

function preload() {
    loadImages();
    game.load.audio('background', ['music/backgroundmusic.mp3', 'music/backgroundmusic.ogg']);
}

function create() {
    gameLoaded = false;
    music = game.add.audio('background');
    console.log();
    music.volume = 0.1;
    //music.play();

    // initialise the socket connection
    socket = io.connect();
    new eventHandlers();


    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    var value;
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        console.log(sParameterName);
        if (sParameterName[0] == "id")
        {
            value =  sParameterName[1];
        }
    }

    quest = new quest();

    loadPlayer(value);
    loadQuests(value);

    //  enable the Arcade Physics system to use collision
    game.physics.startSystem(Phaser.Physics.ARCADE);

    setGameScale();
    makeCanvasResizable();

    // initialise remote players
    remotePlayers = new remotePlayers();

    // initialise the keyboard input keys
    cursors = game.input.keyboard.createCursorKeys();

    chat = new chat();

    animations = new animations();

    challenge = new challenge();
}

var setGameScale = function() {
    game.scale.maxWidth = 2560;
    game.scale.maxHeight = 1440;
    game.scale.minWidth = 960;
    game.scale.minHeight = 540;
};

var makeCanvasResizable = function() {
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    game.scale.setScreenSize();
};

var loadPlayer = function(characterId) {
    // Databank skills
    socket.emit("request skills", { characterId: characterId });
    socket.emit("request character data", { characterId: characterId });
};

var loadQuests = function(characterId) {
    socket.emit("request quest data", { characterId: characterId });
};

var loadMap = function(name, arenaNumber) {
    clearCache();

    if (name == 'mainmap') {
        map = new mainmap();
    }
    else if (name == 'arenamap') {
        map = new arenamap(arenaNumber);
    }
    else if (name == 'block_d') {
        map = new block_d();
    }

    createObstructions();
    enablePhysicsForObstructions();

    map.init();
    animations.init();
};

var clearCache = function() {
    // PHASER SUCKS
    if (background != null) {
        miscellaneous.destroy();
        buildings.destroy();
        flora.destroy();
        entrances.destroy();
        flowers.destroy();
        arena.destroy();
        localSkillGroup.destroy();
        enemySkillGroup.destroy();
        background.destroy();
        //quests.destroy();
        searchQuestsObjects.destroy();
    }
};

var createObstructions = function() {
    miscellaneous = game.add.group();
    flora = game.add.group();
    buildings = game.add.group();
    entrances = game.add.group();
    flowers = game.add.group();
    arena = game.add.group();
    wallsGroup = game.add.group();
    enemySkillGroup = game.add.group();
    localSkillGroup = game.add.group();
    explosions = game.add.group();
    remotePlayersCollision = game.add.group();
    quests = game.add.group();
    searchQuestsObjects = game.add.group();
};

var enablePhysicsForObstructions = function() {
    miscellaneous.enableBody = true;
    buildings.enableBody = true;
    flora.enableBody = true;
    entrances.enableBody = true;
    flowers.enableBody = true;
    arena.enableBody = true;
    wallsGroup.enableBody = true;
    enemySkillGroup.enableBody = true;
    localSkillGroup.enableBody = true;
    remotePlayersCollision.enableBody = true;
    quests.enableBody = true;
    searchQuestsObjects.enableBody = true;
};

function update() {
    if (localPlayer != undefined) {
        if (localPlayer.getBuildingId() != buildingId) {
            if(localPlayer.getBuildingId() > 10) {
                loadMap(maps[2], localPlayer.getBuildingId());
            }
            else {
                if(localPlayer.getBuildingId() == 2) {
                    // FIXME localplayer may never go into buidlingId 2
                    loadMap(maps[localPlayer.getBuildingId()], 0);
                }
                else {
                    loadMap(maps[localPlayer.getBuildingId()], 0);
                }
            }
        }
        game.physics.arcade.collide(localPlayer.getModel(), buildings);
        game.physics.arcade.collide(localPlayer.getModel(), flora);
        game.physics.arcade.collide(localPlayer.getModel(), miscellaneous);
        game.physics.arcade.collide(localPlayer.getModel(), arena);
        game.physics.arcade.collide(localPlayer.getModel(), wallsGroup);

        localPlayer.controls();

        if (map instanceof arenamap == false) {
            chat.checkChatKey();
        } else {
            localPlayer.checkSkillKeys();
            game.world.bringToTop(enemySkillGroup);
            game.world.bringToTop(localSkillGroup);

            map.setManabars();
            map.setHealthbars();

            game.physics.arcade.overlap(localPlayer.getModel(), enemySkillGroup, skillHitsLocalPlayer, null, this);

            game.physics.arcade.overlap(enemySkillGroup, arena, collisionHandler, null, this);
            game.physics.arcade.overlap(localSkillGroup, arena, collisionHandler, null, this);
        }

        map.checkEntranceCollision();

        remotePlayers.drawAndCheckCollision();

        game.world.bringToTop(localPlayer.getModel());

        quest.checkForQuest();
        quest.checkForQuestObjects();
    }
}

function collisionHandler (bullet, wall) {
    /*var explosion = explosions.getFirstExists(false);
    if (explosion) {
        explosion.reset(bullet.x, bullet.y);
        explosion.animations.add('explode', [1, 2, 3, 4, 5, 4, 3, 2, 1, 10], 15, false);
        explosion.animations.play('explode');
    }*/

    bullet.kill();
}

function skillHitsLocalPlayer (opponent, bullet) {
    if(localPlayer.getDefense()) {
        localPlayer.setDefense(false);
    }
    else {
        localPlayer.setHealth(localPlayer.getHealth() - bullet.power);
        socket.emit("player got hit", {
            sendToUserId: challenge.opponent.getUserId(),
            userId: localPlayer.getUserId(),
            health: localPlayer.getHealth()
        });
    }

    bullet.kill();
}